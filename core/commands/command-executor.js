const {
  ReportJsonFileExtractor
} = require('../../utils/report-json-file-extractor');

const {
  ReportOverviewService
} = require('../../lib/services/report-overview.service');

const {
  HtmlReportGeneratorService
} = require('../../lib/services/html-report-generator.service');

const { throwError } = require('../../utils/throw-error');

const CommandExecutor = (function() {
  function CommandExecutor() {}

  CommandExecutor.prototype.execute = function(args, dirname) {
    const { jenkins, app, build } = args;
    if (!args.hasOwnProperty('jenkins') || 
        !args.hasOwnProperty('app')) {
      throwError('Please specify --jenkins, --app and --build arguments!');
    } else {
      ReportJsonFileExtractor.extract().then(
        reports => {
          const overview = ReportOverviewService.compute(reports);
          HtmlReportGeneratorService.generate(app, reports, overview, jenkins, build, dirname);
        },
        function(error) {}
      );
    }
  };

  return new CommandExecutor();
})();

exports.CommandExecutor = CommandExecutor;
