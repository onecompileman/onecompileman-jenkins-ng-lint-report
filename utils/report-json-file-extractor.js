const FileSystem = require('fs');

const ReportJsonFileExtractor = (function() {
    function ReportJsonFileExtractor() {}

    ReportJsonFileExtractor.prototype.extract = function() {
        return new Promise((resolve, reject) => {
              FileSystem.readFile('report.json', function(error, data) {
                  if (error) {
                      console.error('No report.json found, try specifying the path using -p or -path pathname');
                      reject(null);
                  }
                  resolve(JSON.parse(data));
              });
        });
    };

    return new ReportJsonFileExtractor();
})();

exports.ReportJsonFileExtractor = ReportJsonFileExtractor;