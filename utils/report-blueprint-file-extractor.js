const FileSystem = require('fs');

const ReportBlueprintFileExtractor = (function() {
  function ReportBlueprintFileExtractor() {}

  ReportBlueprintFileExtractor.prototype.extract = function(dirname) {
    return new Promise((resolve, reject) => {
      FileSystem.readFile(`${dirname}/blueprints/report.blueprint.html`, function(
        error,
        data
      ) {
        if (error) {
          console.error('No report.blueprint.html is found!!');
          reject(null);
        }
        resolve(data.toString());
      });
    });
  };

  return new ReportBlueprintFileExtractor();
})();

exports.ReportBlueprintFileExtractor = ReportBlueprintFileExtractor;
