const SilentError = require('silent-error');
const chalk = require('chalk');

function throwError(message) {
	console.error(chalk.default.red(message));
}

exports.throwError = throwError;
