const minimist = require('minimist');

const Parser = (function() {
	function Parser() {}

	Parser.prototype.parse = function(args) {
		return minimist(args);
	};

	return new Parser();
})();

exports.Parser = Parser;
