const { CommandExecutor } = require('./core/commands/command-executor');

const { Parser } = require('./utils/parser');

function bootstrap(args) {
    const dirname = __dirname;
    CommandExecutor.execute(Parser.parse(args), dirname);
}

exports.bootstrap = bootstrap;