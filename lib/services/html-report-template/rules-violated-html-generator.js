const RulesViolatedHtmlGenerator = (function() {
  function RulesViolatedHtmlGenerator() {}

  RulesViolatedHtmlGenerator.prototype.generate = function(overviews) {
    return overviews.reduce((acc, overview) => {
      const { count, ruleName, ruleSeverity } = overview;
      return (
        acc +
        `<tr>
                <td>${ruleName}</td>
                <td><b class="red">${ruleSeverity.toUpperCase()}</b></td>
                <td><b class="red">${count}</b></td>
             </tr>`
      );
    }, '');
  };

  return new RulesViolatedHtmlGenerator();
})();

exports.RulesViolatedHtmlGenerator = RulesViolatedHtmlGenerator;
