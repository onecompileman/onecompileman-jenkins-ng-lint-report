const ChartHtmlGenerator = (function() {
  function ChartHtmlGenerator() {}

  ChartHtmlGenerator.prototype.generate = function(overview) {
    const { errors, warnings } = overview;
    if (errors === 0 && warnings === 0) {
      return `datasets: [{
                data: [1],
                backgroundColor: [
                   '#1EA362'
                ]
             }],
             labels: [
                 'success'
             ]`;
    }
    return `datasets: [{
                data: [${errors}, ${warnings}],
                backgroundColor: [
                    '#F64747',
                    '#E9D460'
                ]
             }],
             labels: [
                 'errors',
                 'warnings'
             ]`;
  };

  return new ChartHtmlGenerator();
})();

exports.ChartHtmlGenerator = ChartHtmlGenerator;
