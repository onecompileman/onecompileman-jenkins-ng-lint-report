const LintErrorsHtmlGenerator = (function() {
  function LintErrorsHtmlGenerator() {}

  LintErrorsHtmlGenerator.prototype.generate = function(lintErrors) {
    return lintErrors.reduce((acc, lintError) => {
      const {
        failure,
        name,
        ruleName,
        ruleSeverity,
        startPosition
      } = lintError;
      const color = ruleSeverity === 'ERROR' ? 'red' : 'yellow';
      return (
        acc +
        `<li class="list-group-item">
            <p><i class="bullet ${color}">●</i>
            <b>${ruleName}:</b> &nbsp;
            ${failure}</p>
            <hr>
            <p><b>File: </b>${name.split('workspace')[1]}</p>
            <p><b>Line: </b>${startPosition.line}</p>
            <p><b>Position: </b>${startPosition.position}</p>
        </li>`
      );
    }, '');
  };

  return new LintErrorsHtmlGenerator();
})();

exports.LintErrorsHtmlGenerator = LintErrorsHtmlGenerator;
