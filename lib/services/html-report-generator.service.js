const FileSystem = require('fs');
const mkdirp = require('mkdirp');

const {
  ReportBlueprintFileExtractor
} = require('../../utils/report-blueprint-file-extractor');

const {
  ChartHtmlGenerator
} = require('./html-report-template/chart-html-generator');
const {
  LintErrorsHtmlGenerator
} = require('./html-report-template/lint-errors-html-generator');
const {
  RulesViolatedHtmlGenerator
} = require('./html-report-template/rules-violated-html-generator');

const HtmlReportGeneratorService = (function() {
  function HtmlReportGeneratorService() {}

  HtmlReportGeneratorService.prototype.generate = function(appName, lints, overview, jenkins = '/var/lib/jenkins/reports', build, dirname) {
    
    build = build || generateRandomBuildString();

		const dataChart = ChartHtmlGenerator.generate(overview);
		const lintErrors = LintErrorsHtmlGenerator.generate(lints);
		const rulesViolated = RulesViolatedHtmlGenerator.generate(overview.rulesViolated);

		ReportBlueprintFileExtractor.extract(dirname).then(blueprint => {
			let blueprintString = blueprint;

			blueprintString = blueprintString.replace(/<% app_name %>/g, appName);
			blueprintString = blueprintString.replace(/<% rules_violated %>/g, rulesViolated);
			blueprintString = blueprintString.replace(/<% lint_errors %>/g, lintErrors);
			blueprintString = blueprintString.replace(/<% data_chart %>/g, dataChart);

			mkdirp('./report/', '0755', err => {
				FileSystem.writeFile(`./report/${build}-report.html`, blueprintString, 'utf-8', error => {
					console.log('Successfully generated report.html');
				});
			});
		});
  };

  function generateRandomBuildString() {
    return (Math.random() + 1).toString(36).substring(2);
  }

  return new HtmlReportGeneratorService();
})();

exports.HtmlReportGeneratorService = HtmlReportGeneratorService;
