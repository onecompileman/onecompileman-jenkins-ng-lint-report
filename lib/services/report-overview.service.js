const _ = require('lodash');

const ReportOverviewService = (function() {
    function ReportOverviewService() {}

    ReportOverviewService.prototype.compute = function(reports) {
        return reports.reduce((acc, report) => {
            const { ruleSeverity, ruleName } = report;
            const type = `${ruleSeverity.toLowerCase()}s`;
            const index = _.findIndex(acc.rulesViolated, {ruleName: ruleName});

            acc[type] = acc[type] + 1;
            
            if (index === -1) {
                const rule = {
                    ruleName, 
                    ruleSeverity, 
                    count: 1
                };
                acc.rulesViolated.push(rule);
                return acc;
            }
            acc.rulesViolated[index].count++;
            return acc;
        }, { errors: 0, warnings: 0, rulesViolated: [] });


    };

    return new ReportOverviewService();
})(); 

exports.ReportOverviewService = ReportOverviewService;