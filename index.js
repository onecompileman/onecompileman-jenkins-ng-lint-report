#!/usr/bin/env node
"use strict";

process.title = "ocm-jenkins";

// Grab provided args
const [,,...args] = process.argv;

const { bootstrap } = require('./bootstrap');

bootstrap(args);